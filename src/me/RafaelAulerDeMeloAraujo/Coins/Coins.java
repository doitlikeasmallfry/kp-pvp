package me.RafaelAulerDeMeloAraujo.Coins;



import me.RafaelAulerDeMeloAraujo.main.Main;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

public class Coins
{
  @SuppressWarnings({ "unchecked", "rawtypes" })
public static HashMap<String, Double> bal = new HashMap();
  public static Economy econ = null;
  public static Permission perms = null;
  
  public static HashMap<String, Double> getCoinsMap()
  {
    return bal;
  }
  public static boolean setupEconomy() {
      if (Bukkit.getServer().getPluginManager().getPlugin("Vault") == null) {
          return false;
      }
      RegisteredServiceProvider<Economy> rsp = Bukkit.getServer().getServicesManager().getRegistration(Economy.class);
      if (rsp == null) {
          return false;
      }
      econ = rsp.getProvider();
      return econ != null;
  }
  public static boolean setupPermissions() {
      RegisteredServiceProvider<Permission> rsp = Bukkit.getServer().getServicesManager().getRegistration(Permission.class);
      perms = rsp.getProvider();
      return perms != null;
  }
  
  public static void addCoins(Player player, double amount)
  {
	 econ.depositPlayer(player, amount);
  }
  
  public static Double getCoins(Player player)
  {
	  return econ.getBalance(player);
  }

  public static void removeCoins(Player player, double amount)
  {
	  econ.withdrawPlayer(player, amount);
  }
}
